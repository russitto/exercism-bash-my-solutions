#!/usr/bin/env bash

main () {
  num=$1
  out=""
  if [ $(($num % 3)) == 0 ]; then
    out="Pling"
  fi
  if [ $(($num % 5)) == 0 ]; then
    out="${out}Plang"
  fi
  if [ $(($num % 7)) == 0 ]; then
    out="${out}Plong"
  fi
  if [ "$out" == "" ]; then
    out=$num
  fi
  echo $out
}

main "$@"
