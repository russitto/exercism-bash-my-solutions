#!/usr/bin/env bash

main () {
  if [ $# != 2 ]; then
    echo "Usage: hamming.sh <string1> <string2>"
    exit 1
  fi

  str1=$1
  str2=$2
  len1=`echo -n $str1 | wc -c`
  len2=`echo -n $str2 | wc -c`
  if [ $len1 != $len2 ]; then
    echo "left and right strands must be of equal length"
    exit 1
  fi

  out=0
  for i in `seq 0 $(($len1 - 1))`; do
    c1=`echo ${str1:$i:1}`
    c2=`echo ${str2:$i:1}`
    if [ $c1 != $c2 ]; then
      out=$((out+1))
    fi
  done
  echo $out
}

main "$@"
