#!/usr/bin/env bash

main () {
  if [ $# != 1 ]; then
    echo "Usage: ./error_handling <greetee>"
    exit 1
  fi
  you=$1
  echo "Hello, ${you}"
}

main "$@"
