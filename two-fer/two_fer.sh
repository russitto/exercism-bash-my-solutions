#!/usr/bin/env bash

main () {
  you=$1
  if [ "$you" == "" ]; then
    you=you
  fi
  echo "One for ${you}, one for me."
}

main "$@"
